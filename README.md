Tarzan
====

<b><u>Installation</u></b>


Install dependencies<br/>

`sudo pip install -r requirements.txt`

Run install.py<br/>

`sudo python setup.py install`

<b><u>Run Tarzan</u><b/><br/>
<b>1. Automated Commandline</b>
<br>
`python tarzan.py -W`<br/>
```


_|_|_|_|_|
    _|      _|_|_|  _|  _|_|  _|_|_|_|    _|_|_|  _|_|_|
    _|    _|    _|  _|_|          _|    _|    _|  _|    _|
    _|    _|    _|  _|          _|      _|    _|  _|    _|
    _|      _|_|_|  _|        _|_|_|_|    _|_|_|  _|    _|
                                                           
    

[+] please enter the targets | Default[None] > 


```

<b>2. Tarzan Web API</b>
<br>
`python tarzan.py --start-api`<br>
```

_|_|_|_|_|
    _|      _|_|_|  _|  _|_|  _|_|_|_|    _|_|_|  _|_|_|
    _|    _|    _|  _|_|          _|    _|    _|  _|    _|
    _|    _|    _|  _|          _|      _|    _|  _|    _|
    _|      _|_|_|  _|        _|_|_|_|    _|_|_|  _|    _|
                                                           
    

 * API Key: a8044ee9ac185dc39efe9a31928f9e85
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)

```
The web api is started on port 5000
Enter the address http://127.0.0.1:5000/ on the browser to access web UI
Enter the api key generated on the terminal to Continue using the web UI
<br>
New scan<br>

```
{"backup_ports":null,
"check_ranges":false,
"check_subdomains":false,
"database_host":"",
"database_name":"/home/omniscient/.tarzan/tarzan.db",
"database_password":"",
"database_port":"",
"database_type":"sqlite",
"database_username":"",
"graph_flag":"d3_tree_v2_graph",
"home_path":"/home/omniscient/.tarzan",
"language":"en",
"log_in_file":"/home/omniscient/.tarzan/results/results_2018_12_11_01_29_07_grkbmfwtnj.html",
"methods_args":{"as_user_set":"set_successfully"},
"passwds":null,
"ping_flag":false,
"ports":null,
"profile":"joomla,
wordpress,
scan,
vulnerability,
information_gathering,
brute",
"results_path":"/home/omniscient/.tarzan/results",
"retries":3,
"scan_method":["XSS_protection_vuln",
"ProFTPd_directory_traversal_vuln",
"telnet_brute",
"ssl_certificate_expired_vuln",
"http_form_brute",
"apache_struts_vuln",
"ProFTPd_integer_overflow_vuln",
"self_signed_certificate_vuln",
"xdebug_rce_vuln",
"joomla_user_enum_scan",
"http_basic_auth_brute",
"http_ntlm_brute",
"wp_user_enum_scan",
"ProFTPd_restriction_bypass_vuln",
"http_cors_vuln",
"wp_xmlrpc_bruteforce_vuln",
"wordpress_version_scan",
"clickjacking_vuln",
"ProFTPd_exec_arbitary_vuln",
"cms_detection_scan",
"wordpress_dos_cve_2018_6389_vuln",
"content_security_policy_vuln",
"pma_scan",
"ftp_brute",
"content_type_options_vuln",
"wappalyzer_scan",
"wp_xmlrpc_brute",
"wp_xmlrpc_pingback_vuln",
"smtp_brute",
"drupal_version_scan",
"ProFTPd_memory_leak_vuln",
"wp_plugin_scan",
"ssh_brute",
"drupal_modules_scan",
"heartbleed_vuln",
"Bftpd_memory_leak_vuln",
"CCS_injection_vuln",
"dir_scan",
"viewdns_reverse_ip_lookup_scan",
"Bftpd_parsecmd_overflow_vuln",
"icmp_scan",
"port_scan",
"server_version_vuln",
"x_powered_by_vuln",
"admin_scan",
"wp_timthumbs_scan",
"joomla_version_scan",
"ProFTPd_cpu_consumption_vuln",
"sender_policy_scan",
"ProFTPd_heap_overflow_vuln",
"drupal_theme_scan",
"Bftpd_double_free_vuln",
"weak_signature_algorithm_vuln",
"subdomain_scan",
"Bftpd_remote_dos_vuln",
"wp_theme_scan",
"joomla_template_scan",
"options_method_enabled_vuln",
"ProFTPd_bypass_sqli_protection_vuln"],
"socks_proxy":null,
"targets":["205.204.101.42"],
"thread_number":100,
"thread_number_host":30,
"time_sleep":0,
"timeout_sec":3,
"tmp_path":"/home/omniscient/.tarzan/tmp",
"users":null,
"verbose_level":0}
```

Viewing results and reporting
<br>
*Click on the Crawler button then click on the target to render results on another tab*
<br>
